DECLARE @ShipLineNo INT = 1
DECLARE @ShipNo INT = 3000452

SELECT  
  o.cust_po 'PO',
  od.cust_linno 'POL',
  od.cust_itmno 'PN',
  od.item_no 'MPN',
  co.iso_code 'COO'
FROM shp_hedr s
	INNER JOIN ord_hedr o ON s.order_no = o.order_no
	INNER JOIN ord_detl od ON s.order_no = od.order_no AND od.status != 'V'
	INNER JOIN shp_detl sd ON s.ship_no = sd.ship_no AND sd.status = 'O' AND od.lin_no = @ShipLineNo
	INNER JOIN shp_lot sl ON s.ship_no = sl.ship_no AND od.lin_no = sl.lin_no
	INNER JOIN lot_hedr lh ON sl.lot_no = lh.lot_no
	INNER JOIN lot_info li ON lh.li_int_id = li.li_int_id
	INNER JOIN country co ON li.cnty_of_origin = co.country_cd
WHERE s.ship_no = @ShipNo AND sd.lin_no = @ShipLineNo