--Celestica

--SELECT * FROM LabelZPL

UPDATE LabelZPL
SET
SqlQueryInner =
'SELECT
  RIGHT(li.manu_date_code, 4) ''DateCd'',
  sd.qty_to_shp ''Qty'',
  od.cust_itmno ''PN'',
  od.item_no ''MPN'',
  co.iso_code ''COO'',
  lh.lot_no ''LotCd''
FROM shp_hedr s
	INNER JOIN ord_hedr o ON s.order_no = o.order_no
	INNER JOIN ord_detl od ON s.order_no = od.order_no AND od.status != ''V''
	INNER JOIN shp_detl sd ON s.ship_no = sd.ship_no AND sd.status = ''O'' AND od.lin_no = @ShipLineNo
	INNER JOIN shp_lot sl ON s.ship_no = sl.ship_no AND od.lin_no = sl.lin_no
	INNER JOIN lot_hedr lh ON sl.lot_no = lh.lot_no
	INNER JOIN lot_info li ON lh.li_int_id = li.li_int_id
	INNER JOIN country co ON li.cnty_of_origin = co.country_cd
WHERE s.ship_no = @ShipNo AND sd.lin_no = @ShipLineNo',

InnerLabel = 
'CT~~CD,~CC^~CT~
^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR6,6~SD15^JUS^LRN^CI0^XZ
^XA
^MMT
^PW812
^LL0508
^LS0
^FT17,233^A0N,23,24^FH\^FD(1T) Trace Code: {LotCd}^FS
^BY2,3,41^FT17,292^BCN,,N,N
^FD>:1T{LotCd}^FS
^FT17,42^A0N,23,24^FH\^FD(P) Celestica P/N: {PN}^FS
^BY2,3,41^FT17,101^BCN,,N,N
^FD>:P{PN}^FS
^FT17,137^A0N,23,24^FH\^FD(1P) Manufacturer P/N: {MPN}^FS
^BY2,3,41^FT17,197^BCN,,N,N
^FD>:1P{MPN}^FS
^FT475,424^A0N,23,24^FH\^FD(4L) COO: {COO}^FS
^FT17,328^A0N,23,24^FH\^FD(10D) Date Code: {DateCd}^FS
^BY2,3,41^FT475,483^BCN,,N,N
^FD>:4L{COO}^FS
^BY2,3,41^FT17,388^BCN,,N,N
^FD>:10D{DateCd}^FS
^FT475,328^A0N,23,24^FH\^FD(Q) Quanitity: {Qty}^FS
^BY2,3,41^FT475,388^BCN,,N,N
^FD>:Q{Qty}^FS
^FT17,424^A0N,23,24^FH\^FD(V) Supplier Code: S105850^FS
^BY2,3,41^FT17,483^BCN,,N,N
^FD>:VS105850^FS
^PQ1,0,1,Y^XZ
',

SqlQueryMaster = 
'SELECT
  sd.lin_no ''ShipLn'',
  '''' ''Carton'',
  RIGHT(li.manu_date_code, 4) ''DateCd'',
  sd.qty_to_shp ''Qty'',
  od.cust_itmno ''PN'',
  od.item_no ''MPN'',
  co.iso_code ''COO'',
  lh.lot_no ''Lot'',
  o.cust_po ''PO'',
  od.cust_linno ''POL''
FROM shp_hedr s
	INNER JOIN ord_hedr o ON s.order_no = o.order_no
	INNER JOIN ord_detl od ON s.order_no = od.order_no AND od.status != ''V''
	INNER JOIN shp_detl sd ON s.ship_no = sd.ship_no AND sd.status = ''O'' AND od.lin_no = sd.lin_no
	INNER JOIN shp_lot sl ON s.ship_no = sl.ship_no AND od.lin_no = sl.lin_no
	INNER JOIN lot_hedr lh ON sl.lot_no = lh.lot_no
	INNER JOIN lot_info li ON lh.li_int_id = li.li_int_id
	INNER JOIN country co ON li.cnty_of_origin = co.country_cd
WHERE s.ship_no = @ShipNo',

MasterLabel = 
'CT~~CD,~CC^~CT~
^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR6,6~SD15^JUS^LRN^CI0^XZ
^XA
^MMT
^PW812
^LL0508
^LS0
^FT18,39^A0N,23,24^FH\^FD(3S) Carton: {Carton}^FS
^BY2,3,41^FT18,90^BCN,,N,N
^FD>:3S{Carton}^FS
^FT18,117^A0N,23,24^FH\^FD(P) Celestica P/N: {PN}^FS
^BY2,3,41^FT18,168^BCN,,N,N
^FD>:P{PN}^FS
^FT18,196^A0N,23,24^FH\^FD(1P) Manufacturer P/N: {MPN}^FS
^BY2,3,41^FT18,247^BCN,,N,N
^FD>:1P{MPN}^FS
^FT492,280^A0N,28,28^FH\^FDCOO: {COO}^FS
^FT18,275^A0N,23,24^FH\^FD(10D) Date Code: {DateCd}^FS
^BY2,3,41^FT18,326^BCN,,N,N
^FD>:10D{DateCd}^FS
^FT18,354^A0N,23,24^FH\^FD(Q) Quanitity: {Qty}^FS
^BY2,3,41^FT18,404^BCN,,N,N
^FD>:Q{Qty}^FS
^FT18,432^A0N,23,24^FH\^FD(K) PO: {PO}^FS
^BY2,3,41^FT18,483^BCN,,N,N
^FD>:K{PO}^FS
^FT492,432^A0N,23,24^FH\^FD(4K) PO Line: {POL}^FS
^BY2,3,41^FT492,483^BCN,,N,N
^FD>:4K{POL}^FS
^PQ1,0,1,Y^XZ',
SQLDateFormatterInner = 'SELECT RIGHT(@DateCd, 4)',
SQLDateFormatterMaster = 'SELECT RIGHT(@DateCd, 4)'
WHERE ID = 9

SELECT * FROM LabelZPL