

SELECT * FROM customer WHERE cust_name LIKE '%SEW%'
SELECT * FROM shp_hedr WHERE cust_no = '100007' ORDER BY ship_no DESC

DECLARE @ShipNo INT = 5013272, @ShipLineNo INT = 1

SELECT  
  s.ship_no 'ShipNo',
  sd.lin_no 'ShipLn',
  RIGHT('00000000'+ISNULL(CAST(o.cust_po AS VARCHAR(8)),''),8) +
  RIGHT('00'+ISNULL(CAST(od.cust_linno AS VARCHAR(2)),''),2) 'PoNo',
  od.cust_itmno 'PartNo',
  REPLACE(CAST(lh.lot_no AS VARCHAR(10)), ' ', '') 'LotCd',
  c.our_vend_no 'VendNo'
FROM shp_hedr s
	INNER JOIN customer c ON s.cust_no = c.cust_no 
	INNER JOIN ord_hedr o ON s.order_no = o.order_no
	INNER JOIN ord_detl od ON s.order_no = od.order_no AND od.status != 'V'
	INNER JOIN shp_detl sd ON s.ship_no = sd.ship_no AND od.lin_no = @ShipLineNo
	INNER JOIN shp_lot sl ON s.ship_no = sl.ship_no AND od.lin_no = sl.lin_no
	INNER JOIN lot_hedr lh ON sl.lot_no = lh.lot_no
	INNER JOIN lot_info li ON lh.li_int_id = li.li_int_id
	INNER JOIN country co ON li.cnty_of_origin = co.country_cd
WHERE s.ship_no = @ShipNo AND sd.lin_no = @ShipLineNo


