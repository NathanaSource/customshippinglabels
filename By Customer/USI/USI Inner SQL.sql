SELECT sd.lin_no                  'ShipLn', 
       lh.lot_no                  'LotCd', 
       od.item_no                 'MPN', 
       m.NAME                     'MFG', 
       od.cust_itmno              'PartNo', 
       Isnull(li.manu_lot_no, '') 'MfgLotNo', 
       Isnull(c.iso_code, '')     'COO', 
       li.manu_date_code          'DateCd', 
       CASE WHEN li.rohs_code = 'C' THEN 'RoHS' ELSE '' END AS 'RoHS' 
FROM   shp_hedr s 
       INNER JOIN ord_hedr o ON s.order_no = o.order_no 
       INNER JOIN ord_detl od ON s.order_no = od.order_no AND od.status != 'V' 
       INNER JOIN manufact m ON od.manu_no = m.manu_no 
       INNER JOIN shp_detl AS sd ON s.ship_no = sd.ship_no AND sd.lin_no = od.lin_no       
	   INNER JOIN shp_lot AS sl ON s.ship_no = sl.ship_no AND od.lin_no = sl.lin_no 
       INNER JOIN lot_hedr AS lh ON sl.lot_no = lh.lot_no 
       INNER JOIN lot_info li ON lh.li_int_id = li.li_int_id 
       LEFT OUTER JOIN country c ON li.cnty_of_origin = c.country_cd AND c.active = 1 
WHERE  s.ship_no = @ShipNo AND sd.lin_no = @ShipLineNo 