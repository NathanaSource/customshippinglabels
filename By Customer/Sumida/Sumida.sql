DECLARE @ShipNo INT = 5010667

SELECT sd.cust_itmno 'CPN', 
       i.item_desc   'PartDesc', 
       lh.lot_no     'CN', 
	   lh.lot_no     'LotCd', 
       m.NAME        'MFGName', 
       sd.item_no    'MPN', 
       CASE 
         WHEN Count(DISTINCT RIGHT(li.manu_date_code, 4)) > 1 THEN 
         Min(RIGHT(li.manu_date_code, 4)) + '+' 
         ELSE Min(RIGHT(li.manu_date_code, 4)) 
       END           'DateCd', 
       sd.qty_to_shp 'Qty', 
       CASE WHEN li.rohs_code = 'C' THEN 'Y' ELSE 'N' END  'Rohs',
	   sd.lin_no 'LineNo'
FROM   shp_detl sd
	INNER JOIN shp_hedr s ON sd.ship_no = s.ship_no
	INNER JOIN manufact m ON sd.manu_no = m.manu_no
	INNER JOIN shp_lot sl ON s.ship_no = sl.ship_no AND sd.lin_no = sl.lin_no
	INNER JOIN lot_hedr lh  ON sl.lot_no = lh.lot_no 
    INNER JOIN lot_info li  ON lh.li_int_id = li.li_int_id 
    INNER JOIN item i ON sd.item_no = i.item_no 
WHERE  s.ship_no = @ShipNo AND sd.status != 'V'
GROUP  BY sd.cust_itmno, 
          i.item_desc, 
          M.NAME, 
          sd.item_no, 
          sd.qty_to_shp, 
          li.rohs_code, 
          lh.lot_no,
		  sd.lin_no
