
DECLARE @ShipNo INT = 5010168
DECLARE @ShipLineNo INT = 2

SELECT 
       od.cust_itmno 'CustPN',
	   od.item_no 'MPN',       
       m.name 'MFG',
	   ISNULL(o.cust_po, '') 'PO',
	   i.item_desc 'Desc',
	   CASE WHEN li.rohs_code = 'C' THEN 'RoHS' ELSE '' END AS 'RoHS',
	   ISNULL(c.iso_code, '') 'COO',
	   sd.lin_no 'ShipLn',
	   lh.lot_no 'LotCd'
FROM shp_hedr s
       INNER JOIN ord_hedr o ON s.order_no = o.order_no
       INNER JOIN shp_detl sd ON s.ship_no = sd.ship_no AND sd.lin_no = @ShipLineNo
       INNER JOIN ord_detl od ON s.order_no = od.order_no AND od.lin_no = sd.lin_no AND od.status != 'V'
       INNER JOIN manufact m ON od.manu_no = m.manu_no
       INNER JOIN shp_lot sl ON s.ship_no = sl.ship_no AND od.lin_no = sl.lin_no
	   INNER JOIN lot_hedr lh ON sl.lot_no = lh.lot_no
       INNER JOIN lot_info li ON lh.li_int_id = li.li_int_id
	   INNER JOIN item i ON od.item_no = i.item_no AND od.manu_no = i.manu_no
	   LEFT OUTER JOIN country c ON li.cnty_of_origin = c.country_cd
WHERE s.ship_no = @ShipNo