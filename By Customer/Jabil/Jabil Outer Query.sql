DECLARE @ShipNo INT = 5008822

SELECT sd.lin_no     'ShipLn', 
       s.ship_no     'ShipNo', 
       sd.qty_to_shp 'Qty', 
       od.cust_itmno 'PN', 
       od.item_no    'MPN', 
       lh.lot_no     'LotCd', 
       o.cust_po     'PONo', 
       c.our_vend_no 'SupNo', 
       i.item_desc   'Description', 
       s.st_name     'ShipTo1', 
       s.st_addr1    'ShipTo2', 
       s.st_addr2    'ShipTo3', 
       dbo.ProperCase(s.st_city) + ', ' + s.st_zip 'ShipTo4', 
	   stco.name     'ShipTo5', 
       w.doc_addr1   'ShipFrom1', 
       w.doc_addr2   'ShipFrom2', 
       w.doc_addr3   'ShipFrom3', 
       w.doc_cntry   'ShipFrom4' 
FROM   shp_hedr s 
       INNER JOIN customer c ON s.cust_no = c.cust_no 
       INNER JOIN ord_hedr o  ON s.order_no = o.order_no 
       INNER JOIN ord_detl od ON s.order_no = od.order_no AND od.status != 'V' 
       INNER JOIN shp_detl sd ON s.ship_no = sd.ship_no AND od.lin_no = sd.lin_no 
       INNER JOIN item i ON sd.item_no = i.item_no 
       INNER JOIN shp_lot sl ON s.ship_no = sl.ship_no AND od.lin_no = sl.lin_no 
       INNER JOIN lot_hedr lh ON sl.lot_no = lh.lot_no 
       INNER JOIN lot_info li ON lh.li_int_id = li.li_int_id 
       INNER JOIN country co  ON li.cnty_of_origin = co.country_cd 
	   INNER JOIN country stco  ON s.st_cntry = stco.country_cd
       INNER JOIN whse w ON sd.whse_cd = w.whse_cd 
WHERE  s.ship_no = @ShipNo 