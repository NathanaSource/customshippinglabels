SELECT 
	sd.lin_no 'ShipLn',
	sd.qty_to_shp 'Qty',
	od.cust_itmno 'CustPN',
	li.manu_date_code 'DateCd',
	cust.our_vend_no 'VendCd',
	sl.lot_no 'LotCd',
	od.item_no 'PartNo',	
	m.name 'Mfg',
	i.item_desc 'Desc',
	CASE WHEN li.rohs_code = 'C' THEN 'RoHS' ELSE '' END AS 'ROHS'
FROM shp_hedr s
	INNER JOIN ord_hedr o ON s.order_no = o.order_no
	INNER JOIN ord_detl od ON s.order_no = od.order_no AND od.status != 'V'
	INNER JOIN shp_detl sd ON s.ship_no = sd.ship_no AND sd.status = 'O' AND od.item_no = sd.item_no
	INNER JOIN manufact m ON od.manu_no = m.manu_no
	INNER JOIN shp_lot sl ON s.ship_no = sl.ship_no AND od.lin_no = sl.lin_no
	INNER JOIN lot_hedr lh ON sl.lot_no = lh.lot_no
	INNER JOIN lot_info li ON lh.li_int_id = li.li_int_id	
	INNER JOIN customer cust ON s.cust_no = cust.cust_no
	INNER JOIN item i ON od.item_no = i.item_no AND od.manu_no = i.manu_no
WHERE s.ship_no = @ShipNo


