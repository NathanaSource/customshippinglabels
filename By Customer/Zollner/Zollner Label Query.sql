SELECT DISTINCT  
	od.cust_itmno 'PartNo',
	od.item_no 'MPN',
	'' AS 'OrdCd',
	'' AS 'ManNo',
	'' AS 'ManLoc',
	RIGHT(REPLACE(od.cust_itmno, ' ', ''), 2) AS 'Ind',
	'' AS 'Info',
	ISNULL(dbo.udfSourceConvertDateCode(li.manu_date_code, DEFAULT),'') 'DateCd',
	'' AS 'Expire',
	CASE WHEN li.rohs_code = 'C' THEN 'Y' ELSE 'N' END AS 'RoHS',
	CASE WHEN li.rohs_code = 'C' THEN 'RoHS' ELSE '' END AS 'RoHS2',
	'' AS 'MS',
	ISNULL(o.cust_po, '') 'PO',
	s.ship_no 'ShipNote',
	CASE WHEN v.supp_type IN('M','F') THEN '70030' ELSE '70033' END AS 'SupID',
	'' AS 'PackID',
	'' AS 'Qty',
	CASE WHEN li.manu_lot_no IS NOT NULL AND li.manu_lot_no <> '' THEN 2 ELSE 1 END AS 'Batch',
	sl.lot_no 'Batch1',
	li.manu_lot_no AS 'Batch2',	
	'' AS 'Data'
FROM shp_hedr s
	INNER JOIN ord_hedr o ON s.order_no = o.order_no
	INNER JOIN ord_detl od ON s.order_no = od.order_no AND od.status != 'V'
	INNER JOIN shp_detl sd ON s.ship_no = sd.ship_no AND sd.status = 'O' AND od.item_no = sd.item_no
	INNER JOIN manufact m ON od.manu_no = m.manu_no
	INNER JOIN shp_lot sl ON s.ship_no = sl.ship_no AND od.lin_no = sl.lin_no
	INNER JOIN lot_hedr lh ON sl.lot_no = lh.lot_no
	INNER JOIN lot_info li ON lh.li_int_id = li.li_int_id
	INNER JOIN inv_trx trx ON lh.lot_no = trx.lot_no AND trx.trx_type = 'R'
	INNER JOIN vendor v ON trx.vend_no = v.vend_no
	INNER JOIN customer cust ON s.cust_no = cust.cust_no
WHERE s.ship_no = @ShipNo




