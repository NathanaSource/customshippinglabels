SELECT 
	od.cust_itmno 'PN',
	cust.our_vend_no 'VN',
	sl.lot_no 'LotCd',
	li.manu_date_code 'DateCd',
	CASE WHEN li.rohs_code = 'C' THEN 'RoHS' ELSE '' END AS 'ROHS',
	m.name 'MFG',
	'A' /*Hard coded "A" for "Agent"*/
		+ LEFT(od.cust_itmno, 2) /*First 2 digits of part number*/
		+ LEFT(cust.our_vend_no,6) /*Our vendor number*/
		+ SUBSTRING(li.manu_date_code, 5, 2) /*Week of Date Code*/
		+ SUBSTRING(li.manu_date_code, 3, 2) /*Last 2 of year of Date Code*/
		/*Take the order number, remove the two 0s after the first digit, then include the last 4 digits of the order number, then convert to base 34
		--Example: 2005497 becomes 25497 after removing the two 0s, then converted to base 34 ends up N1X.  Add leading 0s for padding to 4 digits and the final is 0N1X*/
		+ RIGHT('0000' + SourcePortal.dbo.fnDecToBase(LEFT(s.order_no, 1) + RIGHT(s.order_no, 4), 34), 4) 'ID'
FROM shp_hedr s
	INNER JOIN ord_hedr o ON s.order_no = o.order_no
	INNER JOIN ord_detl od ON s.order_no = od.order_no AND od.status != 'V'
	INNER JOIN shp_detl sd ON s.ship_no = sd.ship_no AND sd.status = 'O' AND od.item_no = sd.item_no
	INNER JOIN manufact m ON od.manu_no = m.manu_no
	INNER JOIN shp_lot sl ON s.ship_no = sl.ship_no AND od.lin_no = sl.lin_no
	INNER JOIN lot_hedr lh ON sl.lot_no = lh.lot_no
	INNER JOIN lot_info li ON lh.li_int_id = li.li_int_id	
	INNER JOIN customer cust ON s.cust_no = cust.cust_no
WHERE s.ship_no = @ShipNo
AND sd.lin_no = @ShipLineNo
--@ShipNo

SELECT SUBSTRING(@DateCd, 5, 2) + SUBSTRING(@DateCd, 3, 2) 'DateCd'

