--Sanmina Label

UPDATE LabelZPL SET SqlQueryInner =
'SELECT 
       od.cust_itmno ''CustPN'',
	   od.item_no ''MPN'',       
       m.name ''MFG'',
	   ISNULL(o.cust_po, '''') ''PO'',
	   i.item_desc ''Desc'',
	   CASE WHEN li.rohs_code = ''C'' THEN ''RoHS'' ELSE '''' END AS ''RoHS'',
	   lh.lot_no ''LotCd'',
	   ISNULL(c.iso_code, '''') ''COO''
FROM shp_hedr s
       INNER JOIN ord_hedr o ON s.order_no = o.order_no
       INNER JOIN shp_detl sd ON s.ship_no = sd.ship_no AND sd.lin_no = @ShipLineNo
       INNER JOIN ord_detl od ON s.order_no = od.order_no AND od.lin_no = sd.lin_no AND od.status != ''V''
       INNER JOIN manufact m ON od.manu_no = m.manu_no
       INNER JOIN shp_lot sl ON s.ship_no = sl.ship_no AND od.lin_no = sl.lin_no
	   INNER JOIN lot_hedr lh ON sl.lot_no = lh.lot_no
       INNER JOIN lot_info li ON lh.li_int_id = li.li_int_id
	   INNER JOIN item i ON od.item_no = i.item_no AND od.manu_no = i.manu_no
	   LEFT OUTER JOIN country c ON li.cnty_of_origin = c.country_cd --AND c.active = 1
WHERE s.ship_no =@ShipNo',
InnerLabel = 
'CT~~CD,~CC^~CT~
^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR6,6~SD15^JUS^LRN^CI0^XZ
^XA
^MMT
^PW812
^LL0508
^LS0
^FO640,416^GFA,01920,01920,00020,:Z64:
eJztlLFqwzAQhiUM0VLs1YOJXqGjBoNepcUvYMiSQsEJAWtz1259jY6Z6q1dO5SivEBQN1OMXUmu20i67oX2Btt8/u/XYd8dQn8g8NgFjI5jwKpx3PupA0r8ZKIQ7j2W6MTKY9wcs3HZtcn3DjH+kXSPVfOL75gka7cUa5W7pdgjly47uc4xKYhTYP4ji5yipyqww8qJyVOmppvDpPPqJBYIXfiMoHS1CXSsCFlW1IDfIvBDJbFPdNsxdqQ7xRd9WtwdjZK3A2MDb7tKDIbFmrG2Yez9qmGFaNKijg07TwhjqnzJsCDpSpDM6GLN5AXtcU20zjI+M/5Ask+WNJFmkvZVHWlddKtZ9Gh1GXqzftG9KXBmB+uHDbtsk3wty9ezQ3uTFsJ2ia6ZdpI/daOpWUjzeatdT58lF30leu13MLolUoTJGMu8XqfFdi9RGBDbhwhD7OuPlIAX1BtQD0G9BvUk1LtQj0OzAM0MNFvQDEKzCs40NPvQjtC7BPm7BNo5qFKj9BlV4Q7DKtx1//Fr4wN655/s:B624
^FT502,474^A0N,39,38^FH\^FD{RoHS}^FS
^FT530,287^A0N,25,24^FH\^FDCOO:    {COO}^FS
^FT19,183^A0N,25,24^FH\^FDManufacturer:    {MFG}^FS
^FT19,287^A0N,25,24^FH\^FDDate Code:    {DateCd}^FS
^FT530,360^A0N,25,24^FH\^FDQty:    {Qty}^FS
^BY2,3,41^FT530,335^BCN,,N,N
^FD>:{COO}^FS
^BY2,3,41^FT19,230^BCN,,N,N
^FD>:{MFG}^FS
^FT19,360^A0N,25,24^FH\^FDLot #:    {LotCd}^FS
^FT19,433^A0N,25,24^FH\^FDPO #:    {PO}^FS
^FT19,256^A0N,25,24^FH\^FD{Desc}^FS
^BY2,3,41^FT19,335^BCN,,N,N
^FD>:{DateCd}^FS
^BY2,3,41^FT530,408^BCN,,N,N
^FD>:{Qty}^FS
^FT19,110^A0N,25,24^FH\^FDMFG P/N:    {MPN}^FS
^BY2,3,41^FT19,481^BCN,,N,N
^FD>:{PO}^FS
^BY2,3,41^FT19,408^BCN,,N,N
^FD>:{LotCd}^FS
^FT19,37^A0N,25,24^FH\^FDCust P/N:    {CustPN}^FS
^BY2,3,41^FT19,157^BCN,,N,N
^FD>:{MPN}^FS
^BY2,3,41^FT19,84^BCN,,N,N
^FD>:{CustPN}^FS
^PQ1,0,1,Y^XZ',
MasterLabel = NULL,
SQLQueryMaster = NULL,
SQLDateFormatterInner = 'select @DateCd',
SQLDateFormatterMaster = NULL
WHERE ID = 6

